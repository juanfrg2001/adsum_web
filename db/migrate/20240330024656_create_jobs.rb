class CreateJobs < ActiveRecord::Migration[7.1]
  def change
    create_table :jobs do |t|
      t.references :company, null: false, foreign_key: true
      t.references :city, null: false, foreign_key: true
      t.string :name
      t.string :description
      t.integer :modality
      t.float :salary

      t.timestamps
    end
  end
end
