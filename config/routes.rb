Rails.application.routes.draw do
  devise_for :users
  resources :work_teams, only: [:index]
  resources :contact_us, only: [:index]
  resources :abouts, only: [:index]
  resources :employees, only: [:index, :show, :new, :create]
  resources :home do
    get 'detail_service', on: :collection
  end
  get "up" => "rails/health#show", as: :rails_health_check
  root "home#index"
end
