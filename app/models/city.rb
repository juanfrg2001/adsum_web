class City < ApplicationRecord
  has_many :jobs
  belongs_to :country
end
